import Vue from 'vue'
import store from "./store"
import App from './App'
import mixin from './utils/mixin.js'

import '@/common/layout.scss';

Vue.config.productionTip = false

App.mpType = 'app'
// 全局混入
Vue.mixin(mixin)
const app = new Vue({
  ...App,
  store
})
app.$mount()
