export default {
  methods: {
    back() {
      uni.navigateBack({
      	success:() => {
      		console.log('success')
      	},
      	fail:() => {
      		console.log('fail')
      		uni.switchTab({
      			url:'/tabBar/home/index'
      		})
      	}
      })
    }
  }
}